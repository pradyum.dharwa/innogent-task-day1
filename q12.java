//interface with no body;
// marker interface
import java.util.*;
import java.io.*;
interface Inter
{
	
}
class A implements Serializable // marker interface
{
 transient int id=10;// transient stop the Serializing the value id
	
}
class q12
{
	public static void main(String aar[])throws Exception
	{
		FileOutputStream s=new FileOutputStream("abc1.txt");
		ObjectOutputStream s2=new ObjectOutputStream(s);
		A  a=new A();
		// a.id=10;
		A  b=new A();
		// b.id=20;
		s2.writeObject(a);
		s2.writeObject(b);
		System.out.println("object storation done....");
		s2.close();
		s.close();
   	}
}
class Read1
{
	public static void main(String aar[])throws Exception
	{
		FileInputStream s=new FileInputStream("abc1.txt");
		ObjectInputStream s2=new ObjectInputStream(s);
		try
		{
			A a=(A)s2.readObject();
		System.out.println(a.id);
			A a1=(A)s2.readObject();
		System.out.println(a1.id);
			
		}
		catch(Exception e)
		{
     			
		System.out.println(e);
		}
		finally
		{
			s2.close();
			s.close();
		}
		System.out.println("object storation done....");
		s2.close();
		s.close();
   	}
}
