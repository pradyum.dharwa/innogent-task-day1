class Super1
{
	 static void print()
	{
		System.out.println("super class");
	}
}
class Child extends Super1
{
	 static void print()// here we not overrinding the static method but redefine static method means  
	{
		System.out.println("child class");
	}
}
class q6
{
	public static void main(String ar[])
	{
	    Super1  super1=new Child();	
	    Child child=new Child();	
		super1.print();// print method is not overrinded show super class method called
		Child.print();
	}
}