package innogenttask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Student implements Comparable<Student> {
	int marks;
	String name;
	int id;
	public Student(int id, String name, int marks) {
		super();
		this.marks = marks;
		this.name = name;
		this.id = id;
	}
	
	@Override
	public int compareTo(Student o) {
		
		return this.marks-o.marks;
	}

	@Override
	public String toString() {
		return "Student [marks=" + marks + ", name=" + name + ", id=" + id + "]";
	}
	
     
	

}


class Main
{
    public static void main(String[] args) {
		
//    ArrayList<Student> list=new  ArrayList<>();
//    list.add(new Student(1,"pradyum",40));
//    list.add(new Student(2,"shubhem",30));
//    list.add(new Student(3,"palash",40));
//    list.add(new Student(4,"zzzz",50));
//    list.add(new Student(5,"aaa",60));
//    System.out.println(list);
//   Collections.sort(list);
//   System.out.println(list);
//   Collections.sort(list,new MyComparater());
//   System.out.println(list);
    	
    	
    	List<Integer> list=List.of(1,2,3,4,5,6,7,8,9,10);
//    	Stream<Integer> listStream=list.stream();
    	
    	      list.stream().filter(x->x%2==0).forEach(x->System.out.println(x));
    			
		   }	
}

class MyComparater implements Comparator<Student>
{
     @Override
    public int compare(Student o1, Student o2) {
    	if(o2.marks==o1.marks)
    	{
          return o1.id-o2.id;	
    	}
    	return o2.marks-o1.marks;
    }	
}
