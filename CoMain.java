package innogenttask;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class CoMain {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		Callable c=new MyCollable();
		FutureTask futureTask=new FutureTask(c);
		Thread t=new Thread(futureTask);
		t.start();
		for (int i = 0; i <=10; i++)
		{
			System.out.println("Main Thread==    "+i);
		}
		System.out.println(futureTask.get());
		for (int i = 0; i <=10; i++)
		{
			System.out.println(" "+i);
		}
	}

}
