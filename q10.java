//interface with no body;
// marker interface
import java.util.*;
import java.io.*;
interface Inter
{
	
}
class A implements Serializable // marker interface
{
	int id;
	A(int id)
	{
		this.id=id;
	}
	
}
class q10
{
	public static void main(String aar[])throws Exception
	{
		FileOutputStream fileOutput=new FileOutputStream("abc.txt");
		ObjectOutputStream objectOutput=new ObjectOutputStream(fileOutput);
		A  a=new A(10);
		
		A  b=new A(20);
		
		objectOutput.writeObject(a);
		objectOutput.writeObject(b);
		System.out.println("object storation done....");
		objectOutput.close();
		fileOutput.close();
   	}
}
