//solid properties
       // Single Responsibility 
    // 
class Read
{
	public void read(InputStream s)
	{
		//reading opreation
	}
   
	// public void modifies(InputStream s)// voilet single reposibilty
	// {
		
	// }
}
      
	  
	  // open/close
	  // open for extension and close for modification
	  
	interface Device
	{
		void read();
	}	
	class Console
	{
        void readFromDivice(Device d)
		{
			Divice.read();
		}		
	}
	
	// liskov subsitution
	   // avoid unnecessary inheritence
	   
	   class rectangle
	   {
		   void lenght();
		   void width();
		   void area(); 
	   }
	   class Square extends rectangle // unnecessary extension of rectangle
	   {
		   
	   }
	   
	//interface segregation 
	       // minimum method added to interface for avoid complexity and adding unnecessary method
		 interface animal
         {
			 void eat();
			void  walk();
			// void cloth();unnecessary mehtod
		 }
		 
		   
		 interface human extends animal
		 {
			void  talk();
			void cloth();

		 }		 
		   
		// dependency Inversion

        // more depend on interface not on classes
		
        class Demo
		{
			public void print(List<Integer> l)// using interface insteed of particular class
			{
				l.forEach(x->System.out.println(x));
			}			
		}		
		   
	
