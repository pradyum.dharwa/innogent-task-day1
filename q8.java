 class Circle {
private double radius;
private double area;
  public void setRadius(double radius)
  {
	  this.radius=radius;
	  this.area=3.14*radius*radius;
  }
  public double getArea()
  {
	 return area; 
  }
}
class q8
{
	public static void main(String aar[])
	{
		Circle circle=new Circle();
		circle.setRadius(3);
		System.out.println(circle.getArea());
	}
}