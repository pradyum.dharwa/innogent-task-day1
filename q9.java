
// immutable class

final class A
{
	private final String name;
	private final int id;
	
	A(String name,int id)
	{
      this.name=name;
      this.id=id;	  
	}
	String getName()
	{
		String newName=new String(name);//deep cloning
		
		return newName;
	}
	int getId()
	{
		return id;
	}
	
}

class q9
{
	public static void main(String aar[])
	{
     	A a=new A("pradyum",20);
   System.out.println(a.getName());	
   System.out.println(a.getId());	
		}
}
