// method overloading exa.    ------compile time polymorphism
class A
{
	public void sum(int a,int b)
	{
		System.out.println("sum="+(a+b));
	}
	public void sum(int a,int b,int c)
	{
		System.out.println("sum="+(a+b+c));
	}
}
// method overridding ----- rumtime polymorphism
class B extends A
{
	// overridding sum method
	public  void sum(int i,int j)
	{
		System.out.println("sub..="+(i-j));
	}	
}
class q4
{
	public static void main(String ar[])
	{
     A a=new A();
       a.sum(12,13);	 
       a.sum(12,13,13);	 
	 B b= new B();
	  b.sum(12,13);
	}
	
}