//interface with no body;
// marker interface
import java.util.*;
import java.io.*;
interface Inter
{
	
}
class A implements Serializable // marker interface
{
	int id;
	A(int id)
	{
		this.id=id;
	}
	
}
class q11
{
	public static void main(String aar[])throws Exception
	{
		FileOutputStream fileOutput=new FileOutputStream("abc.txt");
		ObjectOutputStream objectOutput=new ObjectOutputStream(fileOutput);
		A  a=new A(10);
		
		A  b=new A(20);
		
		objectOutput.writeObject(a);
		objectOutput.writeObject(b);
		System.out.println("object storation done....");
		objectOutput.close();
		fileOutput.close();

   	}
}
class ReadObject
{
	public static void main(String aar[])throws Exception
	{
		FileInputStream fileInput=new FileInputStream("abc.txt");
		ObjectInputStream objectInput=new ObjectInputStream(fileInput);
		try
		{
			A a=(A)objectInput.readObject();
		System.out.println(a.id);
			A a1=(A)objectInput.readObject();
		System.out.println(a1.id);
			
		}
		catch(Exception e)
		{
     			
		System.out.println(e);
		}
		finally
		{
			objectInput.close();
			fileInput.close();
		}
		System.out.println("object storation done....");
		
   	}
}
