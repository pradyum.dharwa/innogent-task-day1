class Employee
{
   	String name;
	String gender;
	String department;
	int age;
	int salary;
	int joiningyear;
	Employee(String name,String gender,String department,int age,int salary,int joiningyear)
	{
		this.gender=gender;
		this.name=name;
		this.department=department;
		this.age=age;
		this.salary=salary;
		this.joiningyear=joiningyear;
		
	}
 	String getGender()
	{
		return gender;
	}
	String getName()
	{
		return name;
	}
	String getDepartment()
	{
		return department;
	}
	int getAge()
	{
		return age;
	}
	int getSalary(){
		return salary;
	}
	int getJoiningYear(){
		return joiningyear;
	}
	@Override
	public int hashCode()
	{
		// System.out.println("hashCode");
		return age;
	}
	
	@Override
	public boolean equals(Object o)
	{
		// System.out.println("equals");
		
		Employee employee=(Employee)o;
		return this.getName().equals(employee.getName());
	}
	
	@Override
public String toString()
   {
	return "name="+name+"\tgender="+gender+"\tdepartment="+department+"\tsalary="+salary+"\tage="+age+
	"\tjoiningyear="+joiningyear;
  }
}




