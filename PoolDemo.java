package innogenttask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoolDemo {
  
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int max=2;
		Runnable r=new MyRunable();
		Runnable r1=new MyRunable();
		Runnable r2=new MyRunable();
		ExecutorService pool = Executors.newFixedThreadPool(max);  
       pool.execute(r);
       pool.execute(r1);
       pool.execute(r2);
	}

}

